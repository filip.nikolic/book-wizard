import { SnackbarProvider } from "notistack";
import React from "react";
import Header from "./component/header/Header";
import Wizard from "./component/Wizard";

export default function App() {
  return (
    <SnackbarProvider maxSnack={3}>
      <Header />
      <Wizard />
    </SnackbarProvider>
  );
}
