import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React from "react";
import Label from "../../utils/Labels";
import useStyles from "./HeaderStyle";

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {Label.APP_NAME}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
