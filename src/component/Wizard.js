import { Button, CssBaseline, Grid, Paper } from "@material-ui/core";
import Step from "@material-ui/core/Step";
import StepButton from "@material-ui/core/StepButton";
import Stepper from "@material-ui/core/Stepper";
import Typography from "@material-ui/core/Typography";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import React, { useState } from "react";
import Labels from "../utils/Labels";
import Steps from "../utils/Steps";
import data from "./../utils/dymmyObject.json";
import Gender from "./steps/gender/Gender";
import Information from "./steps/information/Information";
import SubgenderNew from "./steps/new-sungenre/SubgenreNew";
import Subgender from "./steps/subgenre/Subgenre";
import useStyles from "./WizardStyle";

export default function Wizard() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(Steps.GENRE);
  const [selectedGenre, setSelectedGenre] = useState();
  const [subgenreArray, setSubgenreArray] = useState([]);
  const [selectedSubgenre, setSelectedSubgenre] = useState();
  const [informationObject, setInformationObject] = useState({});
  const [isNewSubgenre, setIsNewSubgenre] = useState(false);
  const [isFinished, setIsFinished] = useState(false);

  const steps = () => {
    if ([Steps.SUBGENRE_NEW, Steps.INFORMATION].includes(activeStep)) {
      return [
        Labels.GENRE,
        Labels.SUBGENRE,
        Labels.SUBGENRE_NEW,
        Labels.INFORMATION,
      ];
    }
    return [Labels.GENRE, Labels.SUBGENRE, Labels.UNKNOWN];
  };

  const stepContent = () => {
    if (activeStep === Steps.GENRE) {
      return (
        <Gender
          data={data.genres}
          selectedGenre={selectedGenre}
          setSelectedGenre={changeGenre}
          setActiveStep={setActiveStep}
        />
      );
    } else if (activeStep === Steps.SUBGENRE) {
      return (
        <Subgender
          data={subgenreArray}
          selectedSubgenre={selectedSubgenre}
          setSelectedSubgenre={setSelectedSubgenre}
          setActiveStep={setActiveStep}
          setIsNewSubgenre={setIsNewSubgenre}
        />
      );
    } else if (activeStep === Steps.SUBGENRE_NEW) {
      return (
        <SubgenderNew
          setActiveStep={setActiveStep}
          selectedSubgenre={selectedSubgenre}
          setSelectedSubgenre={setSelectedSubgenre}
        />
      );
    } else if (activeStep === Steps.INFORMATION) {
      return (
        <Information
          informationObject={informationObject}
          setInformationObject={setInformationObject}
          setActiveStep={setActiveStep}
          addBook={addBook}
          selectedSubgenre={selectedSubgenre}
          isNewSubgenre={isNewSubgenre}
        />
      );
    } else {
      return <div />;
    }
  };

  const addBook = () => {
    let object = informationObject;
    object.genre = selectedGenre.id;
    object.subgenre = selectedSubgenre.id
      ? selectedSubgenre.id
      : selectedSubgenre;
    console.log(object);
    setIsFinished(true);
  };

  const changeGenre = (genre) => {
    setSelectedGenre(genre);
    setSubgenreArray(genre.subgenres);
  };

  const handleReset = () => {
    setInformationObject();
    setSelectedGenre();
    setSelectedSubgenre();
    setSubgenreArray();
    setActiveStep(Steps.GENRE);
    setIsFinished(false);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography variant="h6" gutterBottom>
            {!isFinished && Labels.STEP_TITLE}
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              {!isFinished ? (
                <div>
                  <Stepper alternativeLabel nonLinear activeStep={activeStep}>
                    {steps().map((label) => {
                      return (
                        <Step key={label}>
                          <StepButton>{label}</StepButton>
                        </Step>
                      );
                    })}
                  </Stepper>
                  {stepContent()}
                </div>
              ) : (
                <div className={classes.success_container}>
                  <CheckCircleOutlineIcon
                    className={classes.success_icon}
                    color="primary"
                  />
                  <div className={classes.success_text}>
                    {Labels.BOOK_ADD_SUCCESS}
                  </div>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleReset}
                    className={classes.button}
                  >
                    {Labels.ADD_ANOTHER_BOOK}
                  </Button>
                </div>
              )}
            </Grid>
          </Grid>
        </Paper>
      </main>
    </React.Fragment>
  );
}
