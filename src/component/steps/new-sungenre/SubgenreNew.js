import {
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import React from "react";
import Labels from "../../../utils/Labels";
import Steps from "../../../utils/Steps";
import useStyles from "./SubgenreNewStyle";
import { useSnackbar } from "notistack";

export default function SubgenderNew({
  setActiveStep,
  selectedSubgenre,
  setSelectedSubgenre,
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const isFieldPopulate = (field) => {
    return !field || field.trim() === "";
  };

  const showError = (message) => {
    enqueueSnackbar(message, {
      variant: "error",
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
    });
  };

  const addSungenreNew = () => {
    if (!selectedSubgenre || isFieldPopulate(selectedSubgenre["name"])) {
      showError(Labels.INSERT_REQUIRED + Labels.NAME);
      return;
    }
    console.log(selectedSubgenre);
    setActiveStep(Steps.INFORMATION);
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          margin="normal"
          name="name"
          label={Labels.SUBGENRE_NAME}
          value={selectedSubgenre && selectedSubgenre.name}
          onChange={(e) => {
            setSelectedSubgenre({
              ...selectedSubgenre,
              [e.target.name]: e.target.value,
            });
          }}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          control={
            <Checkbox
              variant="outlined"
              margin="normal"
              name="isDescriptionRequired"
              checked={
                selectedSubgenre && selectedSubgenre.isDescriptionRequired
              }
              onChange={(e) => {
                setSelectedSubgenre({
                  ...selectedSubgenre,
                  [e.target.name]: e.target.checked,
                });
              }}
              color="primary"
            />
          }
          label={Labels.SUBGENRE_REQUIRED}
        />
      </Grid>
      <div className={classes.buttons}>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => {
            setSelectedSubgenre();
            setActiveStep(Steps.SUBGENRE);
          }}
          className={classes.button}
        >
          {Labels.BACK}
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={addSungenreNew}
          className={classes.button}
        >
          {Labels.NEXT}
        </Button>
      </div>
    </Grid>
  );
}
