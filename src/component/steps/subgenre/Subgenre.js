import React from "react";
import Button from "@material-ui/core/Button";
import Steps from "../../../utils/Steps";
import Labels from "../../../utils/Labels";
import useStyles from "./SubgenreStyle";
import { Grid } from "@material-ui/core";

export default function Subgender({
  data,
  selectedSubgenre,
  setSelectedSubgenre,
  setActiveStep,
  setIsNewSubgenre,
}) {
  const classes = useStyles();

  return (
    <Grid item margin={2}>
      <div className={classes.container}>
        {data.map((subgenre) => {
          return (
            <Button
              key={subgenre.id}
              className={classes.subgenre}
              variant={
                selectedSubgenre && selectedSubgenre.id === subgenre.id
                  ? "contained"
                  : "outlined"
              }
              color="primary"
              onClick={() => {
                setIsNewSubgenre(false);
                setSelectedSubgenre(subgenre);
              }}
            >
              {subgenre.name}
            </Button>
          );
        })}
        <Button
          variant="outlined"
          color="primary"
          className={classes.subgenre}
          onClick={() => {
            setSelectedSubgenre();
            setIsNewSubgenre(true);
            setActiveStep(Steps.SUBGENRE_NEW);
          }}
        >
          {Labels.ADD_NEW}
        </Button>
      </div>
      <div className={classes.buttons}>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => setActiveStep(Steps.GENRE)}
          className={classes.button}
        >
          {Labels.BACK}
        </Button>

        <Button
          disabled={!selectedSubgenre}
          variant="contained"
          color="primary"
          onClick={() => setActiveStep(Steps.INFORMATION)}
          className={classes.button}
        >
          {Labels.NEXT}
        </Button>
      </div>
    </Grid>
  );
}
