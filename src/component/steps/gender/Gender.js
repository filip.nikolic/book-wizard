import React from "react";
import Button from "@material-ui/core/Button";
import useStyles from "./GenderStyle";
import Steps from "../../../utils/Steps";
import Labels from "./../../../utils/Labels";
import { Grid } from "@material-ui/core";

export default function Gender({
  data,
  selectedGenre,
  setSelectedGenre,
  setActiveStep,
}) {
  const classes = useStyles();

  return (
    <Grid item margin={3}>
      <div className={classes.container}>
        {data.map((genre) => {
          return (
            <Button
              key={genre.id}
              className={classes.genre}
              variant={
                selectedGenre && selectedGenre.id === genre.id
                  ? "contained"
                  : "outlined"
              }
              color="primary"
              onClick={() => {
                setSelectedGenre(genre);
              }}
            >
              {genre.name}
            </Button>
          );
        })}
      </div>
      <div className={classes.buttons}>
        <Button
          disabled={!selectedGenre}
          variant="contained"
          color="primary"
          onClick={() => setActiveStep(Steps.SUBGENRE)}
          className={classes.button}
        >
          {Labels.NEXT}
        </Button>
      </div>
    </Grid>
  );
}
