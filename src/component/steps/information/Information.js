import { Button, Grid, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React from "react";
import {
  authors,
  editionLanguanges,
  formats,
  publishers,
} from "../../../utils/dymmyArray";
import Labels from "../../../utils/Labels";
import Steps from "../../../utils/Steps";
import useStyles from "./InformationStyle";
import { useSnackbar } from "notistack";

export default function Information({
  informationObject,
  setInformationObject,
  setActiveStep,
  addBook,
  selectedSubgenre,
  isNewSubgenre,
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const handleChange = (e) => {
    setInformationObject({
      ...informationObject,
      [e.target.name]: e.target.value,
    });
  };

  const showError = (message) => {
    enqueueSnackbar(message, {
      variant: "error",
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
    });
  };

  const isFieldPopulate = (field) => {
    return !field || field.trim() === "";
  };

  const submit = () => {
    if (isFieldPopulate(informationObject["book_title"])) {
      showError(Labels.INSERT_REQUIRED + Labels.BOOK_TITLE);
      return;
    } else if (!informationObject["author"]) {
      showError(Labels.INSERT_REQUIRED + Labels.AUTHOR);
      return;
    } else if (isFieldPopulate(informationObject["isbn"])) {
      showError(Labels.INSERT_REQUIRED + Labels.ISBN);
      return;
    } else if (!informationObject["publisher"]) {
      showError(Labels.INSERT_REQUIRED + Labels.PUBLISHER);
      return;
    } else if (isFieldPopulate(informationObject["date_publisher"])) {
      showError(Labels.INSERT_REQUIRED + Labels.DATE_PUBLISHER);
      return;
    } else if (isFieldPopulate(informationObject["number_of_page"])) {
      showError(Labels.INSERT_REQUIRED + Labels.NUMBER_OF_PAGE);
      return;
    } else if (!informationObject["format"]) {
      showError(Labels.INSERT_REQUIRED + Labels.FORMAT);
      return;
    } else if (isFieldPopulate(informationObject["edition"])) {
      showError(Labels.INSERT_REQUIRED + Labels.EDITION);
      return;
    } else if (!informationObject["edition_languange"]) {
      showError(Labels.INSERT_REQUIRED + Labels.EDITION_LANGUANGE);
      return;
    } else if (
      selectedSubgenre &&
      selectedSubgenre.isDescriptionRequired &&
      isFieldPopulate(informationObject["description"])
    ) {
      showError(Labels.INSERT_REQUIRED + Labels.DESCRIPTION);
      return;
    }
    addBook();
  };

  return (
    <div>
      <Grid item xs={12}>
        <TextField
          required
          variant="outlined"
          margin="normal"
          name="book_title"
          label={Labels.BOOK_TITLE}
          value={informationObject && informationObject["book_title"]}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Autocomplete
          options={authors}
          getOptionLabel={(option) => option.title}
          style={{ width: "100%" }}
          value={informationObject && informationObject["author"]}
          onChange={(event, value) => {
            setInformationObject({
              ...informationObject,
              author: value.id,
            });
          }}
          renderInput={(params) => (
            <TextField
              required
              {...params}
              label={Labels.AUTHOR}
              variant="outlined"
            />
          )}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          variant="outlined"
          margin="normal"
          name="isbn"
          label={Labels.ISBN}
          value={informationObject && informationObject["isbn"]}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Autocomplete
          options={publishers}
          getOptionLabel={(option) => option.title}
          style={{ width: "100%" }}
          value={informationObject && informationObject["publisher"]}
          onChange={(event, value) => {
            setInformationObject({
              ...informationObject,
              publisher: value.id,
            });
          }}
          renderInput={(params) => (
            <TextField
              required
              {...params}
              label={Labels.PUBLISHER}
              variant="outlined"
            />
          )}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          required
          label={Labels.DATE_PUBLISHER}
          variant="outlined"
          margin="normal"
          name="date_publisher"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          value={informationObject && informationObject["date_publisher"]}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={3}>
        <TextField
          required
          variant="outlined"
          margin="normal"
          name="number_of_page"
          label={Labels.NUMBER_OF_PAGE}
          value={informationObject && informationObject["number_of_page"]}
          onChange={handleChange}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Autocomplete
          options={formats}
          getOptionLabel={(option) => option.title}
          style={{ width: "100%" }}
          value={informationObject && informationObject["format"]}
          onChange={(event, value) => {
            setInformationObject({
              ...informationObject,
              format: value.id,
            });
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              required
              label={Labels.FORMAT}
              variant="outlined"
              margin="normal"
              name="format"
              fullWidth
            />
          )}
        />
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <TextField
            required
            variant="outlined"
            margin="normal"
            name="edition"
            label={Labels.EDITION}
            value={informationObject && informationObject["edition"]}
            onChange={handleChange}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            options={editionLanguanges}
            getOptionLabel={(option) => option.title}
            style={{ width: "100%" }}
            value={informationObject && informationObject["edition_languange"]}
            onChange={(event, value) => {
              setInformationObject({
                ...informationObject,
                edition_languange: value.id,
              });
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                label={Labels.EDITION_LANGUANGE}
                variant="outlined"
                margin="normal"
                fullWidth
              />
            )}
          />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <TextField
          required={selectedSubgenre && selectedSubgenre.isDescriptionRequired}
          label={Labels.DESCRIPTION}
          variant="outlined"
          margin="normal"
          name="description"
          value={informationObject && informationObject["description"]}
          onChange={handleChange}
          multiline
          rows={2}
          rowsMax={4}
          fullWidth
        />
      </Grid>
      <div className={classes.buttons}>
        <Button
          variant="outlined"
          color="primary"
          onClick={() =>
            setActiveStep(isNewSubgenre ? Steps.SUBGENRE_NEW : Steps.SUBGENRE)
          }
          className={classes.button}
        >
          {Labels.BACK}
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={submit}
          className={classes.button}
        >
          {Labels.ADD}
        </Button>
      </div>
    </div>
  );
}
