const authors = [
  {
    id: 1,
    title: "Ivo Andric",
  },
  {
    id: 2,
    title: "Mesa Selimovic",
  },
];

const publishers = [
  {
    id: 1,
    title: "Vulkan",
  },
  {
    id: 2,
    title: "Delfi",
  },
];

const formats = [
  {
    id: 1,
    title: "Roman",
  },
  {
    id: 2,
    title: "Pripovetka",
  },
];

const editionLanguanges = [
  {
    id: 1,
    title: "Serbian",
  },
  {
    id: 2,
    title: "English",
  },
];

export { authors, publishers, formats, editionLanguanges };
