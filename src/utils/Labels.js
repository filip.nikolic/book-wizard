const Labels = {
  APP_NAME: "Book Wizard",

  // step
  GENRE: "Gendre",
  SUBGENRE: "Subgenre",
  SUBGENRE_NEW: "Add new subgenre",
  INFORMATION: "Information",
  UNKNOWN: "...",

  // step title
  STEP_TITLE: "Add book - New book",

  // button
  BACK: "Back",
  NEXT: "Next",
  ADD: "Add",
  ADD_NEW: "Add new",

  // new subgenre
  SUBGENRE_NAME: "Subgenre name",
  SUBGENRE_REQUIRED: "Description is required for this subgenre",
  NAME: "Name",

  // information
  BOOK_TITLE: "Book title",
  AUTHOR: "Author",
  ISBN: "ISBN",
  PUBLISHER: "Publisher",
  DATE_PUBLISHER: "Date published",
  NUMBER_OF_PAGE: "Number of pages",
  FORMAT: "Format",
  EDITION: "Edition",
  EDITION_LANGUANGE: "Edition languange",
  DESCRIPTION: "Description",

  // finished
  BOOK_ADD_SUCCESS: "Book added successfully",
  ADD_ANOTHER_BOOK: "Add another book",

  // message
  INSERT_REQUIRED: "Field is required: ",
};

export default Labels;
